package servlets;

import java.io.IOException;
import java.io.PrintWriter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloServletTest extends Mockito {
	HttpServletRequest request;
	HttpServletResponse response;
	HelloServlet helloServlet;
	PrintWriter writer;
	
	@Before
	public void setUp() throws IOException {
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
		writer = mock(PrintWriter.class);

		when(response.getWriter()).thenReturn(writer);
		
		helloServlet = new HelloServlet();
	}
	
	@After
	public void tearDown() {
		request = null;
		response = null;
		helloServlet = null;
	}
	
	@Test
	public void it_redirects_to_index_on_get_if_name_is_null() throws IOException {		
		when(request.getParameter("name")).thenReturn(null);
		
		helloServlet.doGet(request, response);
		
		verify(response).sendRedirect("/");
	}
	
	@Test
	public void it_redirects_to_index_on_get_if_name_is_empty() throws IOException {
		when(request.getParameter("name")).thenReturn("");
		
		helloServlet.doGet(request, response);
		
		verify(response).sendRedirect("/");
	}
	
	@Test
	public void it_renders_html_response_on_get_if_name_is_provided() throws IOException {
		when(request.getParameter("name")).thenReturn("Name");
		
		helloServlet.doGet(request, response);
		
		verify(response).setContentType("text/html");
		verify(writer).println("<h1>Hello Name!</h1>");
	}
	
	@Test
	public void it_redirects_to_index_on_post_if_name_is_null() throws IOException {
		when(request.getParameter("name")).thenReturn(null);
		
		helloServlet.doPost(request, response);
		
		verify(response).sendRedirect("/");
	}
	
	@Test
	public void it_redirects_to_index_on_post_if_name_is_empty() throws IOException {
		when(request.getParameter("name")).thenReturn("");
		
		helloServlet.doPost(request, response);
		
		verify(response).sendRedirect("/");
	}
	
	@Test
	public void it_renders_html_response_on_post_if_name_is_provided() throws IOException {
		when(request.getParameter("name")).thenReturn("Name");
		
		helloServlet.doPost(request, response);
		
		verify(response).setContentType("text/html");
		verify(writer).println("<h1>Hello Name!</h1>");
	}
}
